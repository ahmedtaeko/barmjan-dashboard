class Errors {

	constructor(){
		this.errors = {};
	}


	get(field){

		if(this.errors[field]){
			return this.errors[field][0];
		}

	}

	has(field){

		if(this.errors){

			return this.errors.hasOwnProperty(field);

		}

		return false;
		
	}

	clear(field){
		if(field){
			delete this.errors[field];
			return;
		}
		this.errors = {};
	}

	any(){

		if(this.errors){
			return Object.keys(this.errors).length > 0;
		}

		return false;
		 
	}

	record(errors){
		this.errors = errors;
	}

}

export default Errors;