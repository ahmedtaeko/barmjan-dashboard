import Errors from './Errors';

class Form{

	 

	constructor(data){

		this.orginalData = data;

		for(let field in data){

			this[field] = data[field];

		}

		this.isLoading = false;

		this.status = '';

		this.errors =  new Errors();
	}

	data(){
		// figure out what the data bayload should be..
		let data = Object.assign({}, this);
		
		delete data.orginalData;
		
		delete data.errors;
		
		delete data.isLoading;

		delete data.status;


		
		return data;
	
	}

	reset(){
	
		for(let field in this.orginalData){
	
			this[field] = '';
	
		}
	
		this.errors.clear();
	
		//this.name = '';
		//behaviour of rest form
	}

	submit(requestType, url, reset = true){
		// return my own promise 

		this.isLoading = true

		return new Promise( (resolve, reject) => {

			axios[requestType](url,this.data()) 
							
							.then( response => {

								if(reset){

									this.onSuccess(response);
									
								}

								resolve(response);
								
								this.isLoading = false
								
								this.status = true // or success
							
							
							
							})
							
							 .catch(  error => {
							
								 this.onFail(error) ;
								 
								 reject(error);
								 
								 this.isLoading = false
								 
								 this.status = false //or error
							
							
							 });

		});
		//behaviour of submit form
		//axios laying here then 
	}

	post(url, reset = true){
	
		return this.submit('post', url, reset);
	
	}

	get(url){
		// 
	}

	delete(url){
		// 
	}

	put(url){
		//
	}

	patch(url, reset = false){

		return this.submit('patch', url, reset);
		

	}


	onSuccess(response){
		
		this.reset();
	
	}

	onFail(error){
	
		 this.errors.record(error.response.data.errors);
	
	}
}


export default Form;