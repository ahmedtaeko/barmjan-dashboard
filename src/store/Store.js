import Vue from 'vue'

import Vuex from 'vuex'
import { resolve, reject } from 'any-promise';

Vue.use(Vuex)

const baseUrl = 'http://127.0.0.1:8000/api';

const endPoints = {

    courses:`${baseUrl}/courses/list`,

    channels:`${baseUrl}/channels`, 

    login: `${baseUrl}/auth/login`,

    logout: `${baseUrl}/auth/logout`,
  

}


function saveToken(data, cb) {
  
  let access_token = data.access_token;
  localStorage.setItem('access_token', access_token )

	// user is auth ^_^
	cb('AuthUser', access_token )

}

function clearToken(cb){

  localStorage.removeItem('access_token')
  
  cb('logout')


}



export const store = new Vuex.Store({

    state: {
      authenticated: false,
      authError: null,
      access_token: localStorage.getItem('access_token') || null ,
      courses: [],
      channels : [],
      whole : []
    },


    mutations:{

      AuthUser(state, access_token){

        state.authenticated = true
        state.authError = null
        state.access_token = access_token

      },

      AuthError(state, e) {
        state.authError = e
      },

      logout(state){

        state.authenticated = false
        state.authError = null
        state.access_token = null

      },

        setChannels(state, channels){

            state.channels = channels;

        },

        filterCourses(state, channel_id){

            if(! channel_id){
      
              state.courses = state.whole;
      
              return ;
      
            }

            state.courses =  _.filter(state.whole, function(course){
      
              return course.channel.id == channel_id;
      
      
            });
      
          },

        setCourses(state, courses){


            // let maped =  _.map(courses,(course) =>{
      
            //     return course[0];

            // } );

            state.courses = courses;
            
            
            state.whole = courses;



        }


    },


    actions:{


      destroyToken({ commit, getters }) {
  
          if( !getters.loggedIn) return;


        return new Promise((resolve, reject) => {

          axios
          .post(endPoints.logout)
          .then(res =>{


            clearToken(commit)

            resolve()

          })
          .catch(({ response }) => {
            
            clearToken(commit)

            reject()

          })




        });

        
      
      
      },


    retrieveToken({commit}, credentials){

      
      return new Promise( (resolve, reject) =>{

        axios.post(endPoints.login, {
          
          username: credentials.username,

          password: credentials.password

        })

            .then( ({data}) => {

              saveToken(data, commit)

              resolve(data)

            })
            .catch( err => {

              console.log(err)
              commit('AuthError', err) 
              
              reject(err)
            
            })





      })

    },


    fetchChannels(context){


        axios.get(endPoints.channels)

          .then( ({data}) => {


            // dd(data.channels, "iam data here");

            context.commit('setChannels', data.channels);
            // this.channels = data.channels;


          })
          .catch(  error => {

            dd(error, 'iam error here of hitting channels end point');

          });

      },

        fetchCourses({commit}){

            axios.get(endPoints.courses)
      
                .then(({data}) => {
      
          
                  console.log('here data courses');
                  console.log(data.courses);

                  commit('setCourses', data.courses)
      
      
                })
      
                .catch(error => {
      
      
                  dd(error , 'iam error here');
      
                })
      
          },



    },

    getters:{

        loggedIn: state => state.access_token != null,

        access_token: state => state.access_token,

        channels: state => state.channels,

        courses: state => state.courses,

        whole: state => state.whole


    }












})